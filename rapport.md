# Outils de productions
## TP4
### GitLab

[Lien du Fork GitLab](https://gitlab.com/JDubocage/tp-rebase-2021)

### Votre branche

4. J'obtient cette erreur : `The upstream branch of your current branch does not match
the name of your current branch` 
5. Réussite : la branche est bien créer sur mon projet perso GitLab (`git push personal`)

### Merge Request

5. La feature branch `1-customize-readme` a bien été merge dans la branche principale `main` puis suprimée.
L'issue qui était reliée à la merge request a été également fermée toute seule.

Julien DUBOCAGE - FIP 1A